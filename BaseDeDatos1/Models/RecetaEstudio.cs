namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RecetaEstudio
    {
        public int ID { get; set; }

        public int RecetaID { get; set; }

        public int EstudioID { get; set; }

        public virtual Estudio Estudio { get; set; }

        public virtual Receta Receta { get; set; }
    }
}
