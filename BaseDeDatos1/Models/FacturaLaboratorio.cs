namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FacturaLaboratorio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FacturaLaboratorio()
        {
            Resultados = new HashSet<Resultado>();
        }

        [Key]
        public int FacturaLabID { get; set; }

        public int LaboratorioID { get; set; }

        [Column(TypeName = "text")]
        public string Detalles { get; set; }

        public double Total { get; set; }

        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        public int RecetaID { get; set; }

        public virtual Laboratorio Laboratorio { get; set; }

        public virtual Receta Receta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Resultado> Resultados { get; set; }
    }
}
