namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FacturaMedicamento")]
    public partial class FacturaMedicamento
    {
        public int ID { get; set; }

        public int FacturaID { get; set; }

        public int MedicamentoID { get; set; }

        public double? Precio { get; set; }

        public int Cantidad { get; set; }

        public virtual Factura Factura { get; set; }

        public virtual Medicamento Medicamento { get; set; }
    }
}
