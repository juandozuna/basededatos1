namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MedicoEspecialidad")]
    public partial class MedicoEspecialidad
    {
        public int ID { get; set; }

        public int MedicoID { get; set; }

        public int EspecialidadID { get; set; }

        public virtual Especialidade Especialidade { get; set; }

        public virtual Medico Medico { get; set; }
    }
}
