namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using BaseDeDatos1.Models.Validations;

    public partial class Medico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Medico()
        {
            Citas = new HashSet<Cita>();
            Licencias = new HashSet<Licencia>();
            MedicoAdmisions = new HashSet<MedicoAdmision>();
            MedicoEspecialidads = new HashSet<MedicoEspecialidad>();
            MedicoHospitals = new HashSet<MedicoHospital>();
            Recetas = new HashSet<Receta>();
        }

        public int MedicoID { get; set; }

        [Required]
        [StringLength(50)]
        [CheckValidName()]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        [CheckValidName()]
        public string Apellido { get; set; }

        [Required]
        [StringLength(15)]
        [MinLength(10), MaxLength(12)]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }

        [Required]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Calle { get; set; }

        [StringLength(50)]
        public string Sector { get; set; }

        [StringLength(50)]
        public string Ciudad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cita> Citas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Licencia> Licencias { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicoAdmision> MedicoAdmisions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicoEspecialidad> MedicoEspecialidads { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicoHospital> MedicoHospitals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Receta> Recetas { get; set; }
    }
}
