namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Factura
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Factura()
        {
            FacturaMedicamentoes = new HashSet<FacturaMedicamento>();
        }

        public int FacturaID { get; set; }

        public double Total { get; set; }

        [Column(TypeName = "text")]
        public string Detalles { get; set; }

        public int? PacienteID { get; set; }

        public int? VisitaID { get; set; }

        public int FarmaciaID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        public int? RecetaID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FacturaMedicamento> FacturaMedicamentoes { get; set; }

        public virtual Farmacia Farmacia { get; set; }

        public virtual Paciente Paciente { get; set; }

        public virtual Receta Receta { get; set; }

        public virtual Visita Visita { get; set; }
    }
}
