namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Admisione
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Admisione()
        {
            EnfermedadAdmisions = new HashSet<EnfermedadAdmision>();
            MedicoAdmisions = new HashSet<MedicoAdmision>();
        }

        [Key]
        public int AdmisionID { get; set; }

        public int PacienteID { get; set; }

        public int VisitaID { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaIngreso { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaSalida { get; set; }

        [Required]
        [StringLength(50)]
        public string Habitacion { get; set; }

        public virtual Paciente Paciente { get; set; }

        public virtual Visita Visita { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EnfermedadAdmision> EnfermedadAdmisions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicoAdmision> MedicoAdmisions { get; set; }
    }
}
