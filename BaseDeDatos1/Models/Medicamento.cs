namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Medicamentos")]
    public partial class Medicamento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Medicamento()
        {
            FacturaMedicamentoes = new HashSet<FacturaMedicamento>();
            FarmaciaMedicamentoes = new HashSet<FarmaciaMedicamento>();
            RecetaMedicamentoes = new HashSet<RecetaMedicamento>();
        }

        public int MedicamentoID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public double? Precio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FacturaMedicamento> FacturaMedicamentoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FarmaciaMedicamento> FarmaciaMedicamentoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecetaMedicamento> RecetaMedicamentoes { get; set; }
    }
}
