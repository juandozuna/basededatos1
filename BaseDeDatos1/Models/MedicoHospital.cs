namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MedicoHospital")]
    public partial class MedicoHospital
    {
        public int ID { get; set; }

        public int MedicoID { get; set; }

        public int HospitalID { get; set; }

        public virtual Hospitale Hospitale { get; set; }

        public virtual Medico Medico { get; set; }
    }
}
