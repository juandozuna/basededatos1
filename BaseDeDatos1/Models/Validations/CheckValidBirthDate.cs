﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseDeDatos1.Models.Validations
{
    public class CheckValidBirthDate : ValidationAttribute
    {
        public CheckValidBirthDate()
          : base("The chosen date is in the future for {0}")
        {

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var val = (DateTime)value;
            val = val.AddHours(-.75);
            if (val <= DateTime.Now)
            {
                var errorMessage = FormatErrorMessage((validationContext.DisplayName));
                return new ValidationResult(errorMessage);
            }
            return ValidationResult.Success;
        }
    }
}