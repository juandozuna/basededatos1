﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseDeDatos1.Models.Validations
{
    public class CheckValidName : ValidationAttribute
    {
        public CheckValidName()
            :base("{0} must be a valid name")
        {

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var text = value.ToString();
            if(text.Any(x => char.IsDigit(x)))
            {
                var errorMessage = FormatErrorMessage((validationContext.DisplayName));
                return new ValidationResult(errorMessage);
            }
            return ValidationResult.Success;
        }
    }
}