﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseDeDatos1.Models.Validations
{
    public class CheckValidCitaDate:ValidationAttribute
    {
        public CheckValidCitaDate()
           :base("The chosen date is in the past for {0}")
        {

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var val = (DateTime)value;
            if(val <= DateTime.Now)
            {
                var errorMessage = FormatErrorMessage((validationContext.DisplayName));
                return new ValidationResult(errorMessage);
            }
            return ValidationResult.Success;
        }
    }
}