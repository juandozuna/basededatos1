namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FarmaciaMedicamento")]
    public partial class FarmaciaMedicamento
    {
        public int ID { get; set; }

        public int FarmaciaID { get; set; }

        public int MedicamentoID { get; set; }

        public virtual Farmacia Farmacia { get; set; }

        public virtual Medicamento Medicamento { get; set; }
    }
}
