namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Estudio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Estudio()
        {
            LaboratorioEstudios = new HashSet<LaboratorioEstudio>();
            RecetaEstudios = new HashSet<RecetaEstudio>();
            Resultados = new HashSet<Resultado>();
        }

        public int EstudioID { get; set; }

        [Required]
        [StringLength(50)]
        public string Titulo { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string rango { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LaboratorioEstudio> LaboratorioEstudios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecetaEstudio> RecetaEstudios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Resultado> Resultados { get; set; }
    }
}
