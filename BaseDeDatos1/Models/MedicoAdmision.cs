namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MedicoAdmision")]
    public partial class MedicoAdmision
    {
        public int ID { get; set; }

        public int MedicoID { get; set; }

        public int AdmisionID { get; set; }

        public virtual Admisione Admisione { get; set; }

        public virtual Medico Medico { get; set; }
    }
}
