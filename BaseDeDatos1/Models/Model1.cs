namespace BaseDeDatos1.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=BaseDeDatosNew")
        {
        }

        public virtual DbSet<Admisione> Admisiones { get; set; }
        public virtual DbSet<Cita> Citas { get; set; }
        public virtual DbSet<EnfermedadAdmision> EnfermedadAdmisions { get; set; }
        public virtual DbSet<Enfermedade> Enfermedades { get; set; }
        public virtual DbSet<Especialidade> Especialidades { get; set; }
        public virtual DbSet<Estudio> Estudios { get; set; }
        public virtual DbSet<FacturaLaboratorio> FacturaLaboratorios { get; set; }
        public virtual DbSet<FacturaMedicamento> FacturaMedicamentoes { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
        public virtual DbSet<FarmaciaMedicamento> FarmaciaMedicamentoes { get; set; }
        public virtual DbSet<Farmacia> Farmacias { get; set; }
        public virtual DbSet<Hospitale> Hospitales { get; set; }
        public virtual DbSet<Laboratorio> Laboratorios { get; set; }
        public virtual DbSet<LaboratorioEstudio> LaboratorioEstudios { get; set; }
        public virtual DbSet<Licencia> Licencias { get; set; }
        public virtual DbSet<Medicamento> Medicamentos { get; set; }
        public virtual DbSet<MedicoAdmision> MedicoAdmisions { get; set; }
        public virtual DbSet<MedicoEspecialidad> MedicoEspecialidads { get; set; }
        public virtual DbSet<MedicoHospital> MedicoHospitals { get; set; }
        public virtual DbSet<Medico> Medicos { get; set; }
        public virtual DbSet<Paciente> Pacientes { get; set; }
        public virtual DbSet<RecetaEstudio> RecetaEstudios { get; set; }
        public virtual DbSet<RecetaMedicamento> RecetaMedicamentoes { get; set; }
        public virtual DbSet<Receta> Recetas { get; set; }
        public virtual DbSet<Resultado> Resultados { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Visita> Visitas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admisione>()
                .Property(e => e.Habitacion)
                .IsUnicode(false);

            modelBuilder.Entity<Admisione>()
                .HasMany(e => e.MedicoAdmisions)
                .WithRequired(e => e.Admisione)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cita>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Cita>()
                .Property(e => e.Notas)
                .IsUnicode(false);

            modelBuilder.Entity<Cita>()
                .HasMany(e => e.Visitas)
                .WithRequired(e => e.Cita)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Enfermedade>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Enfermedade>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Enfermedade>()
                .HasMany(e => e.Enfermedades1)
                .WithOptional(e => e.Enfermedade1)
                .HasForeignKey(e => e.EnfermedadPadreID);

            modelBuilder.Entity<Especialidade>()
                .Property(e => e.Tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Especialidade>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Especialidade>()
                .HasMany(e => e.MedicoEspecialidads)
                .WithRequired(e => e.Especialidade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estudio>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<Estudio>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Estudio>()
                .Property(e => e.rango)
                .IsUnicode(false);

            modelBuilder.Entity<Estudio>()
                .HasMany(e => e.LaboratorioEstudios)
                .WithRequired(e => e.Estudio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estudio>()
                .HasMany(e => e.RecetaEstudios)
                .WithRequired(e => e.Estudio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FacturaLaboratorio>()
                .Property(e => e.Detalles)
                .IsUnicode(false);

            modelBuilder.Entity<FacturaLaboratorio>()
                .HasMany(e => e.Resultados)
                .WithRequired(e => e.FacturaLaboratorio)
                .HasForeignKey(e => e.FacturaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Detalles)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .HasMany(e => e.FacturaMedicamentoes)
                .WithRequired(e => e.Factura)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Farmacia>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Farmacia>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Farmacia>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Farmacia>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Farmacia>()
                .HasMany(e => e.Facturas)
                .WithRequired(e => e.Farmacia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Farmacia>()
                .HasMany(e => e.FarmaciaMedicamentoes)
                .WithRequired(e => e.Farmacia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hospitale>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Hospitale>()
                .Property(e => e.Calle)
                .IsUnicode(false);

            modelBuilder.Entity<Hospitale>()
                .Property(e => e.Ciudad)
                .IsUnicode(false);

            modelBuilder.Entity<Hospitale>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Hospitale>()
                .HasMany(e => e.MedicoHospitals)
                .WithRequired(e => e.Hospitale)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Laboratorio>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Laboratorio>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Laboratorio>()
                .HasMany(e => e.FacturaLaboratorios)
                .WithRequired(e => e.Laboratorio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Laboratorio>()
                .HasMany(e => e.LaboratorioEstudios)
                .WithRequired(e => e.Laboratorio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Licencia>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Medicamento>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Medicamento>()
                .HasMany(e => e.FacturaMedicamentoes)
                .WithRequired(e => e.Medicamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medicamento>()
                .HasMany(e => e.FarmaciaMedicamentoes)
                .WithRequired(e => e.Medicamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medicamento>()
                .HasMany(e => e.RecetaMedicamentoes)
                .WithRequired(e => e.Medicamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Calle)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Sector)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .Property(e => e.Ciudad)
                .IsUnicode(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.Citas)
                .WithRequired(e => e.Medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.Licencias)
                .WithRequired(e => e.Medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.MedicoAdmisions)
                .WithRequired(e => e.Medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.MedicoEspecialidads)
                .WithRequired(e => e.Medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.MedicoHospitals)
                .WithRequired(e => e.Medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.Recetas)
                .WithRequired(e => e.Medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.Apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.Seguro)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.TipoSangre)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Admisiones)
                .WithRequired(e => e.Paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Citas)
                .WithRequired(e => e.Paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Licencias)
                .WithRequired(e => e.Paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Recetas)
                .WithRequired(e => e.Paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Resultados)
                .WithRequired(e => e.Paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Receta>()
                .HasMany(e => e.FacturaLaboratorios)
                .WithRequired(e => e.Receta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Receta>()
                .HasMany(e => e.RecetaEstudios)
                .WithRequired(e => e.Receta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Receta>()
                .HasMany(e => e.RecetaMedicamentoes)
                .WithRequired(e => e.Receta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Resultado>()
                .Property(e => e.ResultadoEstudio)
                .IsUnicode(false);

            modelBuilder.Entity<Visita>()
                .Property(e => e.Tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Visita>()
                .Property(e => e.Notas)
                .IsUnicode(false);

            modelBuilder.Entity<Visita>()
                .HasOptional(e => e.Admisione)
                .WithRequired(e => e.Visita);

            modelBuilder.Entity<Visita>()
                .HasMany(e => e.Licencias)
                .WithRequired(e => e.Visita)
                .WillCascadeOnDelete(false);
        }
    }
}
