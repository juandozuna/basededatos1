namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EnfermedadAdmision")]
    public partial class EnfermedadAdmision
    {
        public int ID { get; set; }

        public int? EnfermedadID { get; set; }

        public int? AdmisionID { get; set; }

        public virtual Admisione Admisione { get; set; }

        public virtual Enfermedade Enfermedade { get; set; }
    }
}
