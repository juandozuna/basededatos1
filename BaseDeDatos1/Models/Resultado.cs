namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Resultados")]
    public partial class Resultado
    {
        public int ResultadoID { get; set; }

        public int FacturaID { get; set; }

        [Required]
        [StringLength(50)]
        public string ResultadoEstudio { get; set; }

        public double? Precio { get; set; }

        public int? EstudioID { get; set; }

        public int PacienteID { get; set; }

        public virtual Estudio Estudio { get; set; }

        public virtual FacturaLaboratorio FacturaLaboratorio { get; set; }

        public virtual Paciente Paciente { get; set; }
    }
}
