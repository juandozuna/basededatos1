namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using BaseDeDatos1.Models.Validations;
    using System.Data.Entity.Spatial;

    public partial class Paciente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Paciente()
        {
            Admisiones = new HashSet<Admisione>();
            Citas = new HashSet<Cita>();
            Facturas = new HashSet<Factura>();
            Licencias = new HashSet<Licencia>();
            Recetas = new HashSet<Receta>();
            Resultados = new HashSet<Resultado>();
        }

        public int PacienteID { get; set; }

        public int? HospitalID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        public string Apellido { get; set; }

        [Required]
        [StringLength(50)]
        [DataType(DataType.PhoneNumber)]
        [MaxLength(15), MinLength(9)]
        public string Telefono { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string Seguro { get; set; }

        [Column(TypeName = "date")]
        [CheckValidBirthDate()]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        [StringLength(50)]
        public string TipoSangre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Admisione> Admisiones { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cita> Citas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factura> Facturas { get; set; }

        public virtual Hospitale Hospitale { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Licencia> Licencias { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Receta> Recetas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Resultado> Resultados { get; set; }
    }
}
