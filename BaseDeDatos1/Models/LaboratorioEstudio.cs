namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LaboratorioEstudio")]
    public partial class LaboratorioEstudio
    {
        public int ID { get; set; }

        public int LaboratorioID { get; set; }

        public int EstudioID { get; set; }

        public double? Precio { get; set; }

        public virtual Estudio Estudio { get; set; }

        public virtual Laboratorio Laboratorio { get; set; }
    }
}
