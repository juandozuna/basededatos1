namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Licencia
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LicenciaID { get; set; }

        public int PacienteID { get; set; }

        public int MedicoID { get; set; }

        public int VisitaID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Descripcion { get; set; }

        public int Dias { get; set; }

        public virtual Medico Medico { get; set; }

        public virtual Paciente Paciente { get; set; }

        public virtual Visita Visita { get; set; }
    }
}
