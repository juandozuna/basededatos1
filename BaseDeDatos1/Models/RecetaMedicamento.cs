namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RecetaMedicamento")]
    public partial class RecetaMedicamento
    {
        public int ID { get; set; }

        public int RecetaID { get; set; }

        public int MedicamentoID { get; set; }

        public virtual Medicamento Medicamento { get; set; }

        public virtual Receta Receta { get; set; }
    }
}
