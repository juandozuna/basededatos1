namespace BaseDeDatos1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Receta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Receta()
        {
            FacturaLaboratorios = new HashSet<FacturaLaboratorio>();
            Facturas = new HashSet<Factura>();
            RecetaEstudios = new HashSet<RecetaEstudio>();
            RecetaMedicamentoes = new HashSet<RecetaMedicamento>();
        }

        public int RecetaID { get; set; }

        public int PacienteID { get; set; }

        public int MedicoID { get; set; }

        public DateTime? FechaReceta { get; set; }

        public int? VisitaID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FacturaLaboratorio> FacturaLaboratorios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factura> Facturas { get; set; }

        public virtual Medico Medico { get; set; }

        public virtual Paciente Paciente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecetaEstudio> RecetaEstudios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecetaMedicamento> RecetaMedicamentoes { get; set; }

        public virtual Visita Visita { get; set; }
    }
}
